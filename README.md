# my-prompt
My prompt that I use on *nix based systems

## Features
* Git prompt
* Naive os type/distro detection
* Generated with https://github.com/jmatth/ezprompt with little adjustment
